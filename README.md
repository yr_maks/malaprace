# Projekt SSP mala prace

This project is a generator of static web pages from Markdown source files.

1. Clone the repository to your local computer:`git clone`
2. Set up the necessary dependencies:`pip install -r requirements.txt`
3. Build the project and generate HTML pages:`mkdocs build`
4. To start a local server:`mkdocs serve`
